const fetch = require('node-fetch');
const fs = require('fs-extra');

// Check each domain
async function checkDomains() {
  const data = fs.readFileSync('./input.txt', 'utf8');
  const domains = data.split('\n');

  let buffer = 'Name,Url,Status,\n';
  const promises = domains.map(async (domain) => {
    const gitUrl = `${domain}/.git`;
    let status = 0;

    try {
      const res = await fetch(gitUrl);
      status = res.status;
      console.log(res)
    } catch (e) {
      // console.log(e);
    }

    return {
      domain,
      gitUrl,
      status,
    }
  });

  try {
    const results = await Promise.all(promises);
    results.forEach(({domain, gitUrl, status}) => {
      buffer += `${domain},${gitUrl},${status},\n`;
    })
  } catch (e) {
    console.log(e);
  }

  fs.writeFileSync('output.csv', buffer);
}

checkDomains();
